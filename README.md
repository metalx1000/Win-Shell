# Win-Shell

Copyright Kris Occhipinti 2022-05-29

(https://filmsbykris.com)

License GPLv3

```bash
curl "https://gitlab.com/metalx1000/Win-Shell/-/raw/master/bin/busybox.exe?ref_type=heads&inline=false" -o busybox.exe
busybox wget "https://gitlab.com/metalx1000/Win-Shell/-/archive/master/Win-Shell-master.zip" -O bb.zip
busybox unzip bb.zip
set PATH=%PATH%;%cd%\Win-Shell-master\bin
del bb.zip
del busybox.exe
busybox sh
```
