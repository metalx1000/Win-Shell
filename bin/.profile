PATH=$HOME/bin:$PATH

alias open="explorer"
alias xdg-open="explorer"
alias fbk="explorer http://filmsbykris.com"
alias vim=vi
alias x=exit

function o(){
  explorer $(fzf)
}

function portableapps(){
  id="$(date +%s)"
  app="$(curl -s "https://portableapps.com/apps"|cut -d\" -f12|grep '/apps/'|fzf)"
  echo "https://portableapps.com$app"
  app="$(curl -s "https://portableapps.com$app"|sed 's/ /%20/g;s/</\n</g'|cut -d\" -f2|grep "downloading"|grep "paf.exe")"
  echo $app
  url="https://portableapps.com$(curl -s "https://portableapps.com$app"|sed 's/ /%20/g;s/</\n</g'|grep redirect|cut -d\" -f2|tail -n1)"
  echo "$url"
  curl -L "$url" -o "/tmp/${id}.exe"
  mkdir -p /tmp/${id}
  cd /tmp/${id}
  7z.exe x -y /tmp/${id}.exe
  explorer *.exe
}


